// arguments object - no longer bound with arrow functions
const add = (a, b) => {
    // console.log(arguments);
    return a + b;
};
console.log(add(55, 1));

// this keyword - no longer bound with arrow functions
const user = {
    name: 'Andy',
    cities: ['Glasgow', 'Edinburgh', 'Cardiff'],
    printPlacesLived() {
        return this.cities.map(city => this.name + ' has worked in ' + city);
    }
};
console.log(user.printPlacesLived());

const multiplier = {
    numbers: [7, 13, 55],
    multiplyBy: 5,
    multiply() {
        return this.numbers.map(number => number * this.multiplyBy);
    }
};
console.log(multiplier.multiply());
