class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddOne = this.handleAddOne.bind(this);
        this.handleRemoveOne = this.handleRemoveOne.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.state = {
            count: 0
        };
    }
    handleAddOne() {
        this.setState(prevState => {
            return {
                count: prevState.count + 1
            };
        });
    }
    handleRemoveOne() {
        this.setState(prevState => {
            return {
                count: prevState.count - 1
            };
        });
    }
    handleReset() {
        this.setState(() => {
            return {
                count: 0
            };
        });
    }
    render() {
        return (
            <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick={this.handleAddOne}>+1</button>
                <button onClick={this.handleRemoveOne}>-1</button>
                <button onClick={this.handleReset}>reset</button>
            </div>
        );
    }
}

// class AddOne extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleAddOne = this.handleAddOne.bind(this);
//     }
//     handleAddOne() {
//         console.log('handleAddOne');
//     }
//     render() {
//         return <button onClick={this.handleAddOne}>+1</button>;
//     }
// }

// class RemoveOne extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleRemoveOne = this.handleRemoveOne.bind(this);
//     }
//     handleRemoveOne() {
//         console.log('handleRemoveOne');
//     }
//     render() {
//         return <button onClick={this.handleRemoveOne}>-1</button>;
//     }
// }

// class Reset extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleReset = this.handleReset.bind(this);
//     }
//     handleReset() {
//         console.log('handleReset');
//     }
//     render() {
//         return <button onClick={this.handleReset}>reset</button>;
//     }
// }

ReactDOM.render(<Counter />, document.getElementById('app'));

// let count = 0;
// const addOne = () => {
//     count++;
//     renderCounterApp();
// };
// const minusOne = () => {
//     count--;
//     renderCounterApp();
// };
// const reset = () => {
//     count = 0;
//     renderCounterApp();
// };

// const appRoot = document.getElementById('app');

// const renderCounterApp = () => {
//     const templateTwo = (
//         <div>
//             <h1>Count: {count}</h1>
//             <button onClick={addOne}>+1</button>
//             <button onClick={minusOne}>-1</button>
//             <button onClick={reset}>Reset</button>
//         </div>
//     );

//     ReactDOM.render(templateTwo, appRoot);
// };
// renderCounterApp();
